package com.example.gridgallery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

public class GalleryBlockAdapter extends ArrayAdapter<GalleryBlock> {
    private Context context;
    private LayoutInflater inflater;
    private int layout;
    private List<GalleryBlock> galleryBlocks;


    public GalleryBlockAdapter(@NonNull Context context, int resource, @NonNull List<GalleryBlock> galleryBlocks) {
        super(context, resource, galleryBlocks);
        this.context = context;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
        this.galleryBlocks = galleryBlocks;

    }

    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = inflater.inflate(this.layout, parent, false);

        ImageView imageView = view.findViewById(R.id.image);
        TextView textView = view.findViewById(R.id.text);

        GalleryBlock galleryBlock = galleryBlocks.get(position);

        imageView.setImageResource(context.getResources().getIdentifier(galleryBlock.getImage(), "drawable", context.getPackageName()));
        textView.setText(galleryBlock.getTitle());
        return view;
    }
}
