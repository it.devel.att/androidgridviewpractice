package com.example.gridgallery;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private GridView galleryView;
    private List<GalleryBlock> galleryBlockList = new ArrayList<>();
    private GalleryBlockAdapter galleryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        galleryBlockList = GalleryBlock.generateBlocks();


        galleryView = findViewById(R.id.gridView);
        galleryAdapter = new GalleryBlockAdapter(this, R.layout.gallery_block, galleryBlockList);
        galleryView.setAdapter(galleryAdapter);
    }
}
