package com.example.gridgallery;

import java.util.ArrayList;
import java.util.List;

public class GalleryBlock {
    private String image;
    private String title;

    public GalleryBlock(String image, String title) {
        this.image = image;
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public static List<GalleryBlock> generateBlocks() {
        List<GalleryBlock> galleryBlocks = new ArrayList<>();
        String[] pictures = new String[]{
                "airplane", "city", "jorney", "nature", "pyramids", "selfie"
        };

        int picture = 0;
        for (int i = 0; i < 18; i++) {
            if (picture == 6) picture = 0;

            galleryBlocks.add(new GalleryBlock(pictures[picture], pictures[picture]));
            picture++;
        }
        return galleryBlocks;
    }

}
